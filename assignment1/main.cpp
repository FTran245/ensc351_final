#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <sys/time.h>
#include <unistd.h>
#include "Trace.h"
#include <stdlib.h>

using namespace std;

mutex g_display_mutex; //for ProcessID


int main()
{

	//cout << "Hello World!"; //testing
	int num;

	TraceClass tr, tr2;

	//Start the add trace
	// tr.trace_start("output");

	// //Processes run here
	// num = tr.add(1000, 2);

	// tr.trace_end();
	// tr.trace_flush();


	//START ADD2 TRACE

	tr2.trace_start("output2");

	//processes run here
	num = tr2.add2(5, 3);
	tr2.trace_end();
	//tr2.trace_flush();

	// tr2.trace_start("malloc");
	// tr2.trace_event_start("another", "what", "\"se\": 22, \"arrrsg\": 232, \"third\": 3");
	// int *foo;
	// foo = new int [50];
	// tr2.trace_event_end("\"arrrsg\": 12");
	// tr2.trace_end();
	// tr2.trace_flush();

//	delete tr2;

	//cout << ::getpid() << endl;

	return 0;

}
