#include <iostream>
#include <string>
#include <sys/time.h>
#include <sstream>
#ifndef TRACE_H
#define TRACE_H

using namespace std;


class trace{

public:

	const char* filename;
	const char* categories;
	const char* ph;
	const char* arguments;

	int ts;

	int pid;
	int tid;

	//Instant events only
	const char* scope;

	//Counter events only
	const char* key;
	const char* value;

	//Object Events
	void* obj_pointer;

	//Constructor
	trace();


};


class TraceClass {


	int ii; //array counter


public:

	trace trace_obj[10000]; //This needs to be first for some reason ...
	int pid;
	int tid;

	string fname; //name of output file

	int st_time;
	int end_time;

	struct timeval start;
	struct timeval stop;

	ostringstream oss;

	bool endFlush = false;	


	TraceClass();

	// ~TraceClass();

	void trace_start(const char* filename);

	void trace_end();

	void trace_event_start(const char* filename, const char* categories, const char* arguments);

	void trace_event_end(const char* arguments);

	void trace_instant_global(const char* name);

	void trace_object_new(const char* name, void* obj_pointer);

	void trace_object_gone(const char* name, void* obj_pointer);

	void trace_counter(const char* name, const char* key, const char* value);

	void trace_flush();


	void find_and_replace(const char* arguments);


	//Dummy functions
	int add(int x, int y);

	int add2(int x, int y);


};


#endif