#include <iostream>
#include <stdio.h>
#include <sys/time.h>
#include <thread> //for getting threadID
#include <sys/types.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)
#include <chrono>
#include <string>
#include <string.h>
#include <sstream>
#include <fstream>
#include <unistd.h> //library for getting processID
#include "Trace.h"

using namespace std;

void TraceClass::trace_flush()
{
	int jj;

	//open file for streaming
	ofstream myFile;
	myFile.open(fname, fstream::app);

	for(jj = 0; jj < ii; jj++)
	{
		// if(jj == 0)
		// {
		// 	myFile << "[ \n";
		// }

		if(trace_obj[jj].ph == "\"B\"")
		{
			if(trace_obj[jj].arguments == NULL) //arguments optional for  duration event, \0 is char* for NULL
			{
				oss << "{\"name\": \"" << trace_obj[jj].filename 
				<< "\", \"cat\": \"" << trace_obj[jj].categories 
				<< "\", \"ph\": " << trace_obj[jj].ph 
				<< ", \"ts\": " << trace_obj[jj].ts 
				<< ", \"pid\": " << trace_obj[jj].pid 
				<< ", \"tid\": " << trace_obj[jj].tid 
				<< "}, \n";					
			}

			else
			{
				oss << "{\"name\": \"" << trace_obj[jj].filename 
				<< "\", \"cat\": \"" << trace_obj[jj].categories 
				<< "\", \"ph\": " << trace_obj[jj].ph 
				<< ", \"ts\": " << trace_obj[jj].ts 
				<< ", \"pid\": " << trace_obj[jj].pid 
				<< ", \"tid\": " << trace_obj[jj].tid
				<< ", \"args\": {" << trace_obj[jj].arguments << "} " << "}, \n";
			}
		}

		else if(trace_obj[jj].ph == "\"E\"")
		{
			if(trace_obj[jj].arguments == NULL) //arguments optional for  duration event
			{
				oss << "{\"ph\": " << trace_obj[jj].ph 
				<< ", \"ts\": " << trace_obj[jj].ts 
				<< ", \"pid\": " << trace_obj[jj].pid 
				<< ", \"tid\": " << trace_obj[jj].tid 
				<< "}, \n";					
			}
			else 
			{
				oss << "{\"ph\": " << trace_obj[jj].ph 
				<< ", \"ts\": " << trace_obj[jj].ts 
				<< ", \"pid\": " << trace_obj[jj].pid 
				<< ", \"tid\": " << trace_obj[jj].tid 
				<< ", \"args\": {"<< trace_obj[jj].arguments << "} " << "}, \n";				
			}
		}

		else if(trace_obj[jj].ph == "\"C\"")
		{
			//Counter event
			oss << "{\"name\": \"" << trace_obj[jj].filename  
			<< "\", \"ph\": " << trace_obj[jj].ph
			<< ", \"pid\": " << trace_obj[jj].pid  
			<< ", \"ts\": " << trace_obj[jj].ts 
			<< ", \"args\": {\""<< trace_obj[jj].key << "\": " << trace_obj[jj].value << "} " << "}, \n";

		}

		else if(trace_obj[jj].ph == "\"i\"")
		{
			//Instant event
			oss << "{\"name\": \"" << trace_obj[jj].filename 
			<< "\", \"ph\": " << trace_obj[jj].ph 
			<< ", \"ts\": " << trace_obj[jj].ts 
			<< ", \"pid\": " << trace_obj[jj].pid 
			<< ", \"tid\": " << trace_obj[jj].tid 
			<< ", \"s\": " << trace_obj[jj].scope << "}, \n";
		}

		else if (trace_obj[jj].ph == "\"N\"" || trace_obj[jj].ph == "\"D\"")
		{
			//Object being or end or snapshot
			oss << "{\"name\": \"" << trace_obj[jj].filename 
			<< "\", \"ph\": " << trace_obj[jj].ph 
			<< ", \"id\": \"" << trace_obj[jj].obj_pointer
			<< "\", \"ts\": " << trace_obj[jj].ts 
			<< ", \"pid\": " << trace_obj[jj].pid 
			<< ", \"tid\": " << trace_obj[jj].tid
			<< "}, \n";		
		}

		else if(trace_obj[jj].ph == "\"O\"")
		{
			//Snapshot objects have arguments
			oss << "{\"name\": \"" << trace_obj[jj].filename 
			<< "\", \"ph\": " << trace_obj[jj].ph 
			<< ", \"id\": \"" << trace_obj[jj].obj_pointer
			<< "\", \"ts\": " << trace_obj[jj].ts 
			<< ", \"pid\": " << trace_obj[jj].pid 
			<< ", \"tid\": " << trace_obj[jj].tid
			<< ", \"args\": {"<< trace_obj[jj].arguments << "} " << "}, \n";
		}
	}
	myFile << oss.str();

	if(endFlush == true)
	{
		myFile << "{\"ph\": " << trace_obj[ii].ph 
		<< ", \"ts\": " << trace_obj[ii].ts 
		<< ", \"pid\": " << trace_obj[ii].pid 
		<< ", \"tid\": " << trace_obj[ii].tid 
		<< "} \n" << "]";		
	}


	//Reset variables
	myFile.close();
	oss.str(string());
	ii = 0;

}

//Start the trace by printing out a dummy trace
void TraceClass::trace_start(const char* filename)
{
	ii = 0;
	fname = filename;

	ofstream myFile;
	myFile.open(fname, fstream::app);

	myFile << "[ \n";

	myFile.close();

	//get process ID and thread ID at start of trace ONLY
	pid = ::getpid();
	tid = ::gettid();

	gettimeofday(&start, NULL);

	st_time = start.tv_usec;

	trace_obj[ii].filename = "DUMMY";
	trace_obj[ii].categories = "EVENT";
	trace_obj[ii].arguments = NULL;
	trace_obj[ii].ts = st_time;
	trace_obj[ii].ph = "\"B\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	ii = ii + 1;
}

//End the trace by completing the dummy trace
void TraceClass::trace_end()
{
	gettimeofday(&stop, NULL);

	end_time = stop.tv_usec;

	trace_obj[ii].filename = "\"DUMMY\"";
	trace_obj[ii].categories = "\"EVENT\"";
	trace_obj[ii].arguments = NULL;
	trace_obj[ii].ts = end_time;
	trace_obj[ii].ph = "\"E\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	endFlush = true;

	//the last flush needs to append shit
	trace_flush();

}

//Start duration event
void TraceClass::trace_event_start(const char* filename, const char* categories, const char* arguments)
{

	if(ii == 9999)
	{
		//Flush trace
		trace_flush();
	}

	//Get the timestamp
	gettimeofday(&start, NULL);
	st_time = start.tv_usec;

	trace_obj[ii].filename = filename;
	trace_obj[ii].categories = categories;
	trace_obj[ii].arguments = arguments;
	trace_obj[ii].ts = st_time;
	trace_obj[ii].ph = "\"B\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	ii = ii + 1; //increment event

}

//End duration event
void TraceClass::trace_event_end(const char* arguments)
{

	if(ii == 9999)
	{
		//Flush trace
		trace_flush();
	}

	gettimeofday(&stop, NULL);
	end_time = stop.tv_usec;

	trace_obj[ii].filename = NULL;
	trace_obj[ii].arguments = arguments;
	trace_obj[ii].ts = end_time;
	trace_obj[ii].ph = "\"E\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	ii = ii + 1;

}


void TraceClass::trace_instant_global(const char* name)
{
	//s specificies the scope of the event
	//s can be g (global), t (thread), p (process)

	if(ii == 9999)
	{
		//Flush trace
		trace_flush();
	}

	//Start the clock
	gettimeofday(&start, NULL);
	st_time = start.tv_usec;

	trace_obj[ii].filename = name;
	trace_obj[ii].ts = st_time;
	trace_obj[ii].ph = "\"i\"";
	trace_obj[ii].scope = "\"g\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	ii = ii + 1;

}

void TraceClass::trace_counter(const char* name, const char* key, const char* value)
{

	if(ii == 9999)
	{
		//Flush trace
		trace_flush();
	}

	gettimeofday(&start, NULL);

	st_time = start.tv_usec;


	trace_obj[ii].filename = name;
	trace_obj[ii].key = key;
	trace_obj[ii].ts = st_time;
	trace_obj[ii].pid = pid;
	trace_obj[ii].value = value;
	trace_obj[ii].ph = "\"C\"";


	ii = ii + 1;
}

void TraceClass::trace_object_new(const char* name, void* obj_pointer)
{

	if(ii == 9999)
	{
		//Flush trace
		trace_flush();
	}

	gettimeofday(&start, NULL);
	st_time = start.tv_usec;

	trace_obj[ii].filename = name;
	trace_obj[ii].obj_pointer = obj_pointer;
	trace_obj[ii].ts = st_time;
	trace_obj[ii].ph = "\"N\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	ii = ii + 1;
}

void TraceClass::trace_object_gone(const char* name, void* obj_pointer)
{

	if(ii == 9999)
	{
		//Flush trace
		trace_flush();
	}

	gettimeofday(&stop, NULL);
	end_time = stop.tv_usec;

	trace_obj[ii].filename = name;
	trace_obj[ii].obj_pointer = obj_pointer;
	trace_obj[ii].ts = end_time;
	trace_obj[ii].ph = "\"D\"";
	trace_obj[ii].tid = tid;
	trace_obj[ii].pid = pid;

	ii = ii + 1;
}

//Constructor
TraceClass::TraceClass()
{
	//intentionally empty
}

trace::trace()
{
	//intentionally empty
}

// //Deconstructor
// TraceClass::~TraceClass()
// {
// 	//empty
// }

void TraceClass::find_and_replace(const char* arguments)
{

}


// ================================================================================ // ========================= //
// ================================================================================ // ========================= //

//Dummy function
int TraceClass::add(int x, int y)
{
	trace_event_start("what", "blah", "\"argumentssss\": 2, \"tee\": 3");

	trace_instant_global("instant");
	trace_instant_global("instant");
	trace_instant_global("instant");

	int result;
	result = x + y;

	if(x > 0)
	{
		result = add(x - 1, y);
		trace_event_end("\"argument\": 68");	
		trace_instant_global("instant");
		trace_instant_global("instant");
		return result;
	}

	else
	{
		trace_event_end("\"placeholder\": 92");
		return result;
	}
}

int TraceClass::add2(int x, int y)
{
	//trace_event_start("another", "what", "\"arrrsg\": 232, \"se\": 22, \"third\": 3");
	trace_event_start("another", "what", NULL);
	// trace_instant_global("instant");
	int result;
	result = x + y;

	TraceClass *object;

	trace_object_new("object", object);
	trace_object_gone("object", object);
	 // trace_instant_global("instant");

	trace_counter("ctr", "what", "3");

	if(x > 0)
	{
		result = add2(x - 1, y);
		//trace_event_end(NULL);
		trace_event_end("\"arrrsg\": 12");
		return result;
	}
	else 
	{
		trace_event_end("\"arrrsg\": 99");
		trace_instant_global("instant");
		//trace_event_end(NULL);
		return result;
	}

}
