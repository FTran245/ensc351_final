#include <iostream>
#include <string>
#include <fstream>
#include <thread>
#include "MapReduce.h"


void firstCount(string filename)
{
	string line;
	ifstream myFile;
	myFile.open(filename);

	if(myFile.is_open())
	{
		while(getline(myFile, line, ' '))
		{
			cout << line << endl;
		}
	}
	else
	{
		cout << "File did not open " << endl;
	}

}