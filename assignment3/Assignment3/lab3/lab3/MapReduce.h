#ifndef MAPREDUCE_H
#define MAPREDUCE_H

#include <iostream>
#include <string>
#include <mutex>
#include <map>
#include <vector>
#include <utility>

using namespace std;

#define NUM 100

extern int count;
extern mutex mtx;

void firstCount(string filename);


void inputReader(string filename, vector<string> &str);

//void Mapper(multimap<string, int> &myMap, vector<string> &str, int i);

std::pair<string, int> Mapper(string &str);

void mapCall(vector<string> &str, vector<pair<string, int>> &keyVal, int i, int numThreads);


//void Reduce(multimap<string, int> &myMap, map<string, int> &newMap);

std::pair<string, int> Reduce(vector<pair<string, int>> temp);

//void output(map<string, int> &newMap);

void output(vector<pair<string, int>> fin);




#endif


