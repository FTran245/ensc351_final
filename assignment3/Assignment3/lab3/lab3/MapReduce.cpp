#include "MapReduce.h"
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>
#include <mutex>




typedef std::pair<std::string, int>myPair;

void firstCount(string filename)
{
	//Declare instream 
	ifstream fin;
	fin.open(filename);

	const int num = 100;
	string line[num];
	map<string, int> c;

	int count = 0;

	//stream in the file and count the number of words
	//does not count \n or whitespace
	for (line; fin >> line[count]; ++count)
	{
		c[line[count]]++;
	}

	fin.close();

	//http://www.techiedelight.com/sort-map-values-cpp/
	vector<myPair> vec;

	std::copy(c.begin(), c.end(), std::back_inserter<std::vector<myPair>>(vec));
	
	std::sort(vec.begin(), vec.end(), [](const myPair &l, const myPair &r) 
	{
		if (l.second != r.second)
			return l.second > r.second;
		return l.first > r.first;
	});
	
	
	// print the vector
	for (auto const &myPair : vec)
	{
		cout << myPair.first << ", " << myPair.second << endl;
	}
}

void inputReader(string filename, vector<string> &str)
{
	//Declare instream 
	ifstream fin;
	fin.open(filename);

	string line[NUM];

	int count = 0;

	for (line; fin >> line[count]; ++count)
	{
		//intentionally empty
	}

	fin.close();

	for (int j = 0; j < count; j++)
	{
		str.push_back(line[j]);
	}
}

std::pair<string, int> Mapper(string &str)
{
	pair<string, int> keyVal;

	keyVal = std::make_pair(str, 1);

	return keyVal;
}

//thread function
void mapCall(vector<string> &str, vector<pair<string, int>> &keyVal, int i, int numThreads)
{
	//create threads to map key value pairs
	for (int jj = i; jj < str.size(); jj = jj + numThreads)
	{
		//need to lock this for the word count
		mtx.lock();
		keyVal.push_back(Mapper(str[jj]));
		mtx.unlock();
	}

}

/*void Mapper(multimap<string, int> &myMap, vector<string> &str, int i)
{
	//Insert the string and a value of 1 into the mmap
	//maps have a pairing, <type1, type2>

	if (i == 0)
	{
		for (int k = 0; k < str.size()/2; k++)
		{
			mtx.lock();
			myMap.insert(pair<string, int>(str[k], 1));
			mtx.unlock();
		}
	}

	if (i == 1)
	{
		for (int k = str.size()/2; k < str.size(); k++)
		{
			mtx.lock();
			myMap.insert(pair<string, int>(str[k], 1));
			mtx.unlock();
		}
	}

}*/

/*void Reduce(multimap<string, int> &myMap, map<string, int> &newMap)
{
	//find duplicates of your keys using .count(), then put those into a new map lol
	//you need to iterate through your mmap like this
	//access the first key using ->first, access the second using ->second

	int count = 0;
	for (multimap<string, int>::iterator it = myMap.begin(); it != myMap.end(); ++it)
	{
		count = myMap.count(it->first);
		newMap.insert(pair<string, int>(it->first, count));
	}

}*/

std::pair<string, int> Reduce(vector<pair<string, int>> temp)
{
	
	int num;
	pair<string, int> final;

	num = temp.size();

	final = std::make_pair(temp[0].first, num);

	return final;

}

/*void output(map<string, int> &newMap)
{
	for (map<string, int>::iterator it = newMap.begin(); it != newMap.end(); ++it)
	{
		cout << it->first << " " << it->second << endl;
	}
}*/

void output(vector<pair<string, int>> fin)
{
	for (int i = 0; i < fin.size(); i++)
	{
		cout << fin[i].first << " " << fin[i].second << endl;
	}

}
