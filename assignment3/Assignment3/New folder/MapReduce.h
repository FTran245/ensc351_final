#ifndef MAPREDUCE_H
#define MAPREDUCE_H

#include <iostream>
#include <string>
#include <mutex>
#include <map>
#include <vector>
#include <utility>

using namespace std;

#define NUM 100


extern int count;
extern mutex mtx;


void inputReader(string filename, vector<int> &in);

std::pair<int, int> Mapper(int &in);


std::pair<int, int> Reduce(vector<pair<int, int>> temp);


void output(vector<pair<int, int>> fin);

void mapCall(int jj, int numThreads, unsigned char* data, int height, int width, int row_padded, FILE* f, vector<pair<int, int>> &fin, double &whiteCount);


#endif


