#include <iostream>
#include <thread>
#include <fstream>
#include <chrono>
#include <mutex>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include "MapReduce.h"

using namespace std;
int count = 0;
mutex mtx;


typedef std::chrono::high_resolution_clock Clock;

int main()
{

	//firstCount();

	const int numThreads = 4;
	thread myThread[numThreads];

	double whiteCount = 0;
	vector<pair<int, int>> fin;

	double percentage;

	FILE* f = fopen("Untitled.bmp", "rb");

	if (f == NULL)
	{
		throw "Argument Exception";
	}
		
	//https://stackoverflow.com/questions/9296059/read-pixel-value-in-bmp-file
	// read in 24 bit .bmp image
	//you have to create it using paint
	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, f); // read the 54-byte header

	// extract image height and width from header
	int width = *(int*)&info[18];
	int height = *(int*)&info[22];

	cout << endl;
	cout << "Width: " << width << endl;
	cout << "Height: " << height << endl;

	int row_padded = (width * 3 + 3) & (~3);
	unsigned char* data = new unsigned char[row_padded];

	//timer
	auto t1 = Clock::now();

	for (int jj = 0; jj < numThreads; jj++)
	{
		myThread[jj] = thread(mapCall, jj, numThreads, data, height, width, row_padded, f, ref(fin), ref(whiteCount));
	}

	for (int jj = 0; jj < numThreads; jj++)
	{
		myThread[jj].join();
	}

	//timer
	auto t2 = Clock::now();

	percentage = whiteCount / (width*height);

	std::cout << "Delta t2-t1: "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds" << std::endl;


	cout << "Total White Pixels = " << whiteCount << endl;
	cout << "Percentage of White in the Image = %" << percentage * 100 << endl;

	fclose(f);

	output(fin);


	return 0;
}




