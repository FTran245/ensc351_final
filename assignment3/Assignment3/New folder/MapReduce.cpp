#include "MapReduce.h"
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>
#include <mutex>


typedef std::pair<std::string, int>myPair;

/*

void inputReader(string filename, vector<int> &in)
{
//Declare instream
ifstream fin;
fin.open(filename);

string line[NUM];

int count = 0;

for (line; fin >> line[count]; ++count)
{
//intentionally empty
}

fin.close();

for (int j = 0; j < count; j++)
{
str.push_back(line[j]);
}
}

*/



std::pair<int, int> Mapper(int &in)
{
	pair<int, int> keyVal;

	keyVal = std::make_pair(in, 1);

	return keyVal;
}

void mapCall(int jj, int numThreads, unsigned char* data, int height, int width, int row_padded, FILE* f, vector<pair<int, int>> &fin, double &whiteCount)
{
	//count the number of white pixels row by row
	//increment by a factor of numThreads
	for (int i = jj; i < height; i = i + numThreads)
	{
		vector<pair<int, int>> keyVal;
		fread(data, sizeof(unsigned char), row_padded, f);
		for (int j = 0; j < width * 3; j += 3)
		{
			//	cout << "R: " << (int)data[j] << " G: " << (int)data[j + 1] << " B: " << (int)data[j + 2] << endl;
			if ((int)data[j] == 255 && (int)data[j + 1] == 255 && (int)data[j + 2] == 255)
			{
				keyVal.push_back(Mapper(i));
			}
		}

		if (keyVal.size() != 0)
		{
			//need to lock whiteCount variable
			mtx.lock();
			fin.push_back(Reduce(keyVal));
			whiteCount += keyVal.size();
			mtx.unlock();
		}
	}
}


std::pair<int, int> Reduce(vector<pair<int, int>> temp)
{

	int num;
	pair<int, int> final;

	num = temp.size();

	final = std::make_pair(temp[0].first, num);

	return final;

}



void output(vector<pair<int, int>> fin)
{
	//	for (int i = 0; i < fin.size(); i++)
	//	{
	//		cout << fin[i].first << " " << fin[i].second << endl;
	//	}

}
