#ifndef THREAD_H
#define THREAD_H

using namespace std;

//METHOD 1 
// #define NUM 100
// extern bool wait;
// extern int doorCount;
// extern pthread_mutex_t mu;

// void *door1(void *count);


//METHOD 2
#define NUM 100
extern pthread_mutex_t mu2;
extern int doorCount2;
extern bool wait2[NUM];
extern long ii;

extern bool timer;

void *door2(void *ptr);

//METHOD 3
// #define NUM 100
// extern pthread_mutex_t mu3;
// extern long ii;
// extern long ticket[NUM];
// extern bool wait3;
// extern int doorCount3;
// extern int j;

// extern int next_ticket;
// extern int now_serving;
// // extern int my_ticket;

// void *door4(void *ptr);

// void ticketLock_init();

// void ticketLock_acquire();

// void ticketLock_release();

// int fetch_and_inc(int next_ticket);


// void *door3(void *ptr);



#endif


