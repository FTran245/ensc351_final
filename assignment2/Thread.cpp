#include <iostream>
#include <chrono>
#include <cmath>
#include <pthread.h>
#include <fstream>
// #include <sys/time.h>
#include "Thread.h"



//METHOD 1

// void *door1(void *count)
// {
// 	long c = (long) count;

// 	//function lock to wait for all threads to be created

//  	while (wait == true) 
//  	{
//  		//empty
//  	} 

//  	auto start = std::chrono::system_clock::now();
 	
//  	pthread_mutex_lock(&mu);

//  	auto end = std::chrono::system_clock::now();
// 	std::chrono::duration<double> elapsed_seconds = end-start;
// 	double micros = std::chrono::duration_cast<std::chrono::microseconds>(elapsed_seconds).count();
//  	// cout << micros << " " << c << endl;
//  	doorCount = doorCount + 1;

// 	ofstream myFile;
// 	myFile.open("meth1_us", fstream::app);
// 	myFile << micros << ", " << c << ", " << endl;

//  	pthread_mutex_unlock(&mu);
// }

//METHOD2
void *door2(void *ptr)
{
	long c = (long) ptr;
	
	auto start = std::chrono::high_resolution_clock::now();


	ofstream myFile;
	myFile.open("meth2_us", fstream::app);

 // 	pthread_mutex_lock(&mu2);
 // 	for(int ii = 0; ii < NUM; ii++)
	// {
	// 	cout << wait2[ii] << " " << c << endl;
	// }
 // 	pthread_mutex_unlock(&mu2);


	while(wait2[c] == true)
	{
		//do nothing
	}

 	// pthread_mutex_lock(&mu2);


 	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
	double micros = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_seconds).count();
	// cout << micros << " " << c << endl;

	doorCount2 = doorCount2 + 1;


	myFile << micros << ", " << c << ", " << endl;
	wait2[doorCount2] = false;
 	// pthread_mutex_unlock(&mu2);
}

//METHOD3
// void *door4(void *ptr)
// {
// 	long c = (long) ptr;
	
// 	ofstream myFile;
// 	myFile.open("meth3_us", fstream::app);

// 	while(wait3 == true)
// 	{
// 		//do nothing
// 	}
// 	auto start = std::chrono::high_resolution_clock::now();

// 	ticketLock_init();


// 	ticketLock_acquire();
	
// 	pthread_mutex_lock(&mu3);
//  	auto end = std::chrono::high_resolution_clock::now();
// 	std::chrono::duration<double> elapsed_seconds = end-start;
// 	double micros = std::chrono::duration_cast<std::chrono::microseconds>(elapsed_seconds).count();
// 	// cout << micros << " " << c << endl;

// 	doorCount3 = doorCount3 + 1;
// 	myFile << micros << ", " << c << endl;
// 	myFile.close();
// 	pthread_mutex_unlock(&mu3);
//  	ticketLock_release();

// }



// void ticketLock_init()
// {
// 	now_serving = next_ticket = 0;
// }

// void ticketLock_acquire()
// {
// 	// pthread_mutex_lock(&mu3);
// 	int my_ticket = fetch_and_inc(next_ticket);
// 	// cout << my_ticket << endl;
// 	// pthread_mutex_unlock(&mu3);


// 	while(now_serving != my_ticket)
// 	{
// 		//empty
// 	}
// }

// void ticketLock_release()
// {
// 	++now_serving;
// }

// int fetch_and_inc(int next_ticket)
// {
// 	// int n = *next_ticket;
// 	// next_ticket = next_ticket + 1;
	
// 	return next_ticket++;
// }





