#include <iostream>
#include "Thread.h"
#include <pthread.h>
#include <fstream>
#include <unistd.h>
#include <chrono>
#include <cstdlib>

using namespace std;


//Global variables, need to declare in Header file, then define somewhere
//METHOD 1

// bool wait = true;
// pthread_mutex_t mu;
// int doorCount = 0;

//METHOD 2
int doorCount2 = 0;
pthread_mutex_t mu2;
bool wait2[NUM];
long ii; 
double t;

bool timer;


//METHOD 3
// pthread_mutex_t mu3;
// bool wait3 = true;
// long ii;
// long ticket[NUM];
// int doorCount3 = 0;
// int j;

// int next_ticket;
// int now_serving;

//compile with g++ -std=c++11 main.cpp Thread.cpp -lpthread
// valgrind -v --tool=helgrind ./a.out
int main()
{

	// METHOD 1 =================== // =================== // =================== //
	// METHOD 1 =================== // =================== // =================== // 
	// long ii; //use long instead of int to avoid losing data during casting
	// pthread_t th[NUM];
	// int rc;
	// ofstream total1;
	// total1.open("meth1_total", fstream::app);

	// METHOD 2 =================== // =================== // =================== //
	// METHOD 2 =================== // =================== // =================== // 

	int rc;
	pthread_t th[NUM];
	bool timer = false;

	ofstream total2;
	total2.open("meth2_total", fstream::app);

	// METHOD 3 =================== // =================== // =================== //
	// METHOD 3 =================== // =================== // =================== // 
	// int rc;
	// pthread_t th[NUM];
	// long ticket[NUM];
	// wait3 = true;
	// j = 0;
	// ofstream total;
	// total.open("meth3_total", fstream::app);


	// METHOD 1 =================== // =================== // =================== //
	// METHOD 1 =================== // =================== // =================== // 
	// for(ii = 0; ii < NUM; ii++)
	// {	
	// 	//pthread_create(pointer to thread, attribute (can be NULL), function (just the name), argument (cast as void))
	// 	rc = pthread_create(&th[ii], NULL, door1, (void *)ii);
	// }

	// wait = false;
	// auto start = std::chrono::high_resolution_clock::now();


	// for(ii = 0; ii < NUM; ii++)
	// {
	// 	pthread_join(th[ii], NULL);
	// }

 // 	auto end = std::chrono::high_resolution_clock::now();
	// std::chrono::duration<double> elapsed_seconds = end-start;
	// double micros_o = std::chrono::duration_cast<std::chrono::microseconds>(elapsed_seconds).count();

	// total1 << micros_o << endl;

	// cout << "First method: "<< doorCount << endl;

	// METHOD 1 =================== // =================== // =================== //
	// METHOD 1 =================== // =================== // =================== // 


	// METHOD 2 =================== // =================== // =================== //
	// METHOD 2 =================== // =================== // =================== // 

	for(int jj = 0; jj < NUM; jj++)
	{
		wait2[jj] = true;
	}


	for(ii = 0; ii < NUM; ii++)
	{	
		//pthread_create(pointer to thread, attribute (can be NULL), function (just the name), argument (cast as void))
		rc = pthread_create(&th[ii], NULL, door2, (void*) ii);
	}

	// sleep(1);

	timer = true;
	wait2[0] = false;

	auto start = std::chrono::high_resolution_clock::now();

	for(ii = 0; ii < NUM; ii++)
	{
		pthread_join(th[ii], NULL);
	}

	 auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
	double micros_o = std::chrono::duration_cast<std::chrono::microseconds>(elapsed_seconds).count();
// cout << micros_o << endl;
	total2 << micros_o << endl;

	cout << "Second method " << doorCount2 << endl;

	// METHOD 3 =================== // =================== // =================== //
	// METHOD 3 =================== // =================== // =================== // 

	// for(int kk = 0; kk < NUM; kk++)
	// {
	// 	ticket[kk] = '\0';
	// }
	

	// for(ii = 0; ii < NUM; ii++)
	// {	
	// 	//pthread_create(pointer to thread, attribute (can be NULL), function (just the name), argument (cast as void))
	// 	rc = pthread_create(&th[ii], NULL, door4, (void*) ii);
	// }

	// // sleep(1);
	

	// //release all threads
	// wait3 = false;
	// auto start = std::chrono::high_resolution_clock::now();


	// // for(ii = 0; ii < NUM; ii++)
	// // {
	// // 	cout << ticket[ii] << endl;
	// // }

	// for(ii = 0; ii < NUM; ii++)
	// {
	// 	pthread_join(th[ii], NULL);
	// }

 // 	auto end = std::chrono::high_resolution_clock::now();
	// std::chrono::duration<double> elapsed_seconds = end-start;
	// double micros_o = std::chrono::duration_cast<std::chrono::microseconds>(elapsed_seconds).count();

	// total << micros_o << endl;

	// cout << "Third method " << doorCount3 << endl;


	return 0;
}


