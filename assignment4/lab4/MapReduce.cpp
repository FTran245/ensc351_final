#include "MapReduce.h"
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>
#include <omp.h>
#include <mutex>


typedef std::pair<std::string, int>myPair;

void firstCount(string filename)
{
	//Declare instream 
	ifstream fin;
	fin.open(filename);

	const int num = 100;
	string line[num];
	map<string, int> c;

	int count = 0;

	//stream in the file and count the number of words
	//does not count \n or whitespace
	for (line; fin >> line[count]; ++count)
	{
		c[line[count]]++;
	}

	fin.close();

	//http://www.techiedelight.com/sort-map-values-cpp/
	vector<myPair> vec;

	std::copy(c.begin(), c.end(), std::back_inserter<std::vector<myPair>>(vec));
	
	std::sort(vec.begin(), vec.end(), [](const myPair &l, const myPair &r) 
	{
		if (l.second != r.second)
			return l.second > r.second;
		return l.first > r.first;
	});
	
	
	// print the vector
	for (auto const &myPair : vec)
	{
		cout << myPair.first << ", " << myPair.second << endl;
	}
}

void inputReader(string filename, vector<string> &str)
{
	//Declare instream 
	ifstream fin;
	fin.open(filename);

	string line[NUM];

	int count = 0;

	for (line; fin >> line[count]; ++count)
	{
		//intentionally empty
	}

	fin.close();

	for (int j = 0; j < count; j++)
	{
		str.push_back(line[j]);
	}
}

std::pair<string, int> Mapper(string &str)
{
	pair<string, int> keyVal;

	keyVal = std::make_pair(str, 1);

	return keyVal;
}

//thread function
void mapCall(vector<string> &str, vector<pair<string, int>> &keyVal, int i, int numThreads)
{
	//create threads to map key value pairs
	for (int jj = i; jj < str.size(); jj = jj + numThreads)
	{
		//need to lock this for the word count
		//use this as the critical section instead of mtx.lock()
		//only 1 thread executes this section at a time
#pragma omp critical
		{
			keyVal.push_back(Mapper(str[jj]));
		}
	}
}



std::pair<string, int> Reduce(vector<pair<string, int>> temp)
{
	
	int num;
	pair<string, int> final;

	num = temp.size();

	final = std::make_pair(temp[0].first, num);

	return final;

}


void output(vector<pair<string, int>> fin)
{
	for (int i = 0; i < fin.size(); i++)
	{
		cout << fin[i].first << " " << fin[i].second << endl;
	}

}
