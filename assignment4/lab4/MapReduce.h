#ifndef MAPREDUCE_H
#define MAPREDUCE_H

#include <iostream>
#include <string>
#include <mutex>
#include <map>
#include <vector>
#include <utility>

using namespace std;

#define NUM 100000 //size of string array, depends on how large the text file is


void firstCount(string filename);


void inputReader(string filename, vector<string> &str);


std::pair<string, int> Mapper(string &str);

void mapCall(vector<string> &str, vector<pair<string, int>> &keyVal, int i, int numThreads);


std::pair<string, int> Reduce(vector<pair<string, int>> temp);

void output(vector<pair<string, int>> fin);


#endif


