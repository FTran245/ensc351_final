#include <iostream>
#include <thread>
#include <fstream>
#include <chrono>
#include <mutex>
#include <vector>
#include <omp.h>
#include <algorithm>
#include "MapReduce.h"


using namespace std;

typedef std::chrono::high_resolution_clock Clock;

//compile with
//g++ -std=c++11 main.cpp MapReduce.cpp -fopenmp
int main()
{
	/*
	single threaded word count implementation 
	super horrible
	but still better than the multithreaded word count lol
	*/
	/*
	//timer
	auto t1 = Clock::now();
	firstCount("testing1.txt");
	//timer
	auto t2 = Clock::now();

	std::cout << "Delta t2-t1: "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds" << std::endl;
	*/
	

	vector<string> str;

	vector<pair<string, int>> keyVal;
	multimap<string, int> myMap;

	map<string, int> newMap;

	//reads text from a file and stores in string vector
	inputReader("testing1.txt", str);

	//timer
	auto t1 = Clock::now();

	//using openMP
	//parallel section
	//use this instead of C++ thread()
	//you can also specify how many threads you want, I just didn't do it here
#pragma omp parallel
	{
		//if you want variables to be UNIQUE to the thread, you must declare them in the parallel section
		int id = omp_get_thread_num();
		int total = omp_get_num_threads();

		mapCall(str, keyVal, id, total);
	}


	//timer
	auto t2 = Clock::now();

	//sort the vector of key value pairs by their string values using lambda function
	std::sort(keyVal.begin(), keyVal.end(), [](pair<string, int> &left, pair<string, int> &right) 
	{
		if (left.first != right.first)
			return left.first > right.first;
		return left.second > right.second;
	});

	//place vector into a map
	int repeat;
	for (int ii = 0; ii < keyVal.size(); ii++)
	{
		myMap.insert(keyVal[ii]);
	}

	//place map into multimap lol
	for (multimap<string, int>::iterator it = myMap.begin(); it != myMap.end(); ++it)
	{
		repeat = myMap.count(it->first);
		newMap.insert(pair<string, int>(it->first, repeat));
	}

	//loop through and use count() to find duplicates
	//each loop, create a temp vector and fill it depending on how man duplicates you find
	//for example "test test test dog dog house"
	//test = 3, dog = 2, house = 1, create temp vector for each of these with those lengths
	//run Reduce on the temp vector
	//super roundabout and useless 
	vector<pair<string, int>> fin;
	int rep;
	for (multimap<string, int>::iterator it = newMap.begin(); it != newMap.end(); ++it)
	{

		rep = it->second;
		vector<pair<string, int>> temp;
		for (int kk = 0; kk < rep; kk++)
		{
			temp.push_back(std::make_pair(it->first, 1));
		}

		//the actual Reduce() call
		fin.push_back(Reduce(temp));
	}


	output(fin);

	std::cout << "Elapsed time: "
		<< std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
		<< " microseconds" << std::endl;


	return 0;
}


