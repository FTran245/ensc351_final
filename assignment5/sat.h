#ifndef SAT_H
#define SAT_H
#include <vector>
#include <sstream>
#include <mutex>

using namespace std;

extern long long int bcount; //backtrack count
extern bool printFlag; //flag to signal printing every 2 seconds
extern bool solutionFound; //flag to signal a solution has been found
extern const int numThreads; //number of threads

//initialize numbers matrix with data
void fillData(vector<vector<int>> &numbers, stringstream oss[], int count);

//encode true/false values into input matrix
void encodeTrueFalse(vector<vector<int>> &input, vector<vector<int>> numbers, int nvar, int ncl);

//initialize/reset satisfied array
void initSat(bool sat[]);
void resetSat(bool sat[]);

//guess a new solution
void changeSolution(vector<int> &sol, int bcount, int nvar);

//begin checking/guessing process
void guessSolution(vector<vector<int>> input, int nvar, int ncl, int i);

//mutxes
extern mutex printMutex; //may not need this one
extern mutex mtx;

//print number of backtracks every 2 seconds
void printBackTrack();

#endif