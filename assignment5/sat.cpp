#include "sat.h"
#include <vector>
#include <cmath>
#include <bitset>
#include <iostream>
#include <mutex>
#include <algorithm>
#include <thread>
#include <sstream>
#include <chrono>
#include <cstdlib>
#include <unistd.h>

using namespace std;

void fillData(vector<vector<int>> &numbers, stringstream oss[], int count)
{
	int jj = 0;
	int num;

	for (int ii = 4; ii < count; ii++)
	{
		oss[ii] >> num;
		if (num == 0)
		{
			jj++;
		}
		else
		{
			numbers[jj].push_back(num);
		}
	}
}

void encodeTrueFalse(vector<vector<int>> &input, vector<vector<int>> numbers, int nvar, int ncl)
{
	int x;

	//place 2's for variables that aren't included
	for (int i = 0; i < ncl; i++)
	{
		input[i].resize(nvar, 2);
	}

	//create matrix of 0's and 1's
	for (int j = 0; j < ncl; j++)
	{
		for (int k = 0; k < numbers[j].size(); k++)
		{
			x = numbers[j][k];
			if (x < 0)
			{
				input[j][abs(x) - 1] = 0; //0 for negative
			}
			else
			{
				input[j][abs(x) - 1] = 1;
			}
		}
	}
}


void initSat(bool sat[])
{
	//initialize sat array
	for (int kk = 0; kk < 100; kk++)
	{
		sat[kk] = false;
	}
}

void resetSat(bool sat[])
{
	//reset entire satisfaction array
	//1024 is the max number of clauses allowed
	for (int ii = 0; ii < 1024; ii++)
	{
		sat[ii] = false;
	}
}

void changeSolution(vector<int> &sol, int bcount, int nvar)
{
	bitset<1024> bits(bcount);

	for (int x = 0; x < nvar; x++)
	{
		sol[x] = bits[x];
	}
}


void guessSolution(vector<vector<int>> input, int nvar, int ncl, int i)
{
	vector<int> sol; //proposed solution
	bool sat[1024]; //1024 is the max number of clauses
	int varNum = 0;
	//long long int bcount = 0;
	int solIdx = nvar - 1;
	int64_t maxComb = pow(2, nvar);
	int64_t loopCount = 0;
	sol.resize(nvar, 0);
	int clauseNum = 0;
	long long int comb = i;

	//initialize sat array
	initSat(sat);

	while (clauseNum != ncl && solutionFound == false)
	{
		if (loopCount > maxComb)
		{
			cout << "no solution " << endl;
			break;
		}

		for (varNum = 0; varNum < nvar; varNum++)
		{
			//if found a match
			if (sol[varNum] == input[clauseNum][varNum])
			{
				sat[clauseNum] = true;
				break;
			}
		}

		if (sat[clauseNum] == false)
		{
			//reset clause index, change solution, and update backtrack count
			mtx.lock();
			bcount++;
			mtx.unlock();
			comb = comb + numThreads;
			changeSolution(sol, comb, nvar);
			clauseNum = 0;

			//reset sat array and recheck all clauses
			resetSat(sat);
			loopCount++;
			continue;
		}

		else
		{
			clauseNum++;
		}
	}

	mtx.lock();
	//print solution
	if (clauseNum == ncl)
	{
		solutionFound = true;
		cout << "solved by thread: " << i << endl;
		for (int t = 0; t < sol.size(); t++)
		{
			cout << sol[t] << " ";
		}
		printFlag = false;
		cout << endl << "backtrack count seen by thread " << i << ": " << bcount << endl;
	}

	mtx.unlock();
}

void printBackTrack()
{
	while (printFlag == true)
	{
		//this_thread::sleep_for(chrono::seconds(2));
		sleep(2); //use this for linux
		//Sleep(2000); //wait for 2 seconds
		//printMutex.lock();
		cout << "number of backtracks so far: " << bcount << endl;
		//printMutex.unlock();
	}

}

