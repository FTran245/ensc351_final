#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <thread>
#include <vector>
#include <cstdint>
#include <mutex>
#include <bitset>
#include "sat.h"


using namespace std;

mutex printMutex;
mutex mtx;
long long int bcount = 0;
bool printFlag = true;
const int numThreads = 2;
bool solutionFound = false;

int main()
{
	string filename;
	string line[1024];
	stringstream oss[1024];

	vector<vector<int>> input; //true/false matrix
	vector<vector<int>> numbers; //actual data


	int count = 0; //number of elements that are read
	int nvar; //number of variables
	int ncl; //number of clauses

	filename = "testing1.txt";

	ifstream myFile;
	myFile.open(filename);

	//read in file to stringstream
	for (line; myFile >> line[count]; ++count)
	{
		oss[count] << line[count];
	}

	myFile.close();

	//stream in number of variables and number of clauses
	oss[2] >> nvar;
	oss[3] >> ncl;

	//need to resize the number of vectors in your vector before assigning
	numbers.resize(ncl);
	
	//fill vector with data
	fillData(numbers, oss, count);

	//resize the input vector
	input.resize(ncl);

	//encode 0 for negative, 1 for positive, and 2 for unincluded variables
	encodeTrueFalse(input, numbers, nvar, ncl);

	thread t_print;
	
	printFlag = true;
	t_print = thread(printBackTrack); //print backtrack count every 2 seconds

	//const int numThreads = 4;
	thread th[numThreads]; //declare threads

	for (int i = 0; i < numThreads; i++)
	{
		th[i] = thread(guessSolution, input, nvar, ncl, i);
	}

	//join threads
	for (int j = 0; j < numThreads; j++)
	{
		th[j].join();
	}

	t_print.join();

	return 0;
}


